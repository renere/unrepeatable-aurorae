# Unrepeatable Aurorae.

To install, enter the following into your terminal: 
```bash
git clone https://gitlab.com/renere/unrepeatable-aurorae Unrepeatable && mv Unrepeatable ~/.local/share/aurorae/themes/
```

If the theme looks identical to the Plastik theme, ensure the folder is named 'Unrepeatable' and not 'unrepeatable-aurorae'.
